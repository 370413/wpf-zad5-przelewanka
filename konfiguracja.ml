type konfiguracja = int array

let create l = l

let strip_from_zeros acc el =
    if el = (0, 0) then acc else Array.append acc [|el|]

let create2 l = 
    let nl = Array.fold_left strip_from_zeros [||] l in
    (create (Array.map fst nl), create (Array.map snd nl))

let size = Array.length

let get = Array.get

let set u s w = begin
    let nu = Array.copy u in
    nu.(s) <- w;
    nu
end

let map f u = begin
    let s = size u in
    let nu = Array.make s 0 in
    for i = 0 to (s - 1) do
        nu.(i) <- f (u.(i))
    done;
    nu
end

let iter f u = 
    for i = 0 to (size u) - 1 do
        f i (u.(i))
    done
