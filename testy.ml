open Przelewanka

(* sposób użycia: w interaktywnej konsoli:
    #load "konfiguracja.cmo";;
    #load "przelewanka.cmo";;
    #use "testy.ml";;
*)

let test =
    begin
        assert(0 = (przelewanka [|(2, 0)|]));
        assert(1 = (przelewanka [|(2, 2)|]));
        assert(-1 = (przelewanka [|(2, 1)|]));

        let b = [|(7, 1); (1, 1); (2, 2); (3, 2); (4, 2); (5, 3); (1, 1); (1, 1); (1, 1)|] in
        let c = Array.append b [|(5, 9)|] in
        let d = Array.append b [|(5, 2)|] in
        
        assert(-1 = (przelewanka c));
        assert(9 = (przelewanka b));
        assert(10 = (przelewanka d)); (* uwaga: czas wykonania u mnie ~ 1/2 minuty *)
        
        print_int 42;
        
        print_int (przelewanka [| (0,0); (0,0); (0,0); (300000,151515); (1,0); (0,0) |]) (* powinno zająć niewiele czasu *)
    end
