(* 
    Autor: Tomasz Necio
    Code review: Karol Waszczuk
    
    idea: 
    BFS na grafie, którego korzeniem jest konfiguracja początkowa a dziećmi
    każdego wierzchołka są konfiguracje które można uzyskać z rodzica po
    pojedynczej modyfikacji:
    wchodzimy po kolei do kolejnych poziomów grafu
    sprawdzamy czy któraś z tamtejszych konfiguracji jest celem
    jeżeli nie to powtarzamy poziom niżej
    jeżeli zbiór jest pusty, a nie znaleźliśmy celu to zwracamy -1
    
    (konfiguracja – poziomy wody w szklankach,
     cel – docelowa konfiguracja)
     
    implementacja mocno imperatywna, ponieważ wersja napisana w całości
    funkcyjnie okazała się dużo wolniejsza
*)

(* 
   dokładna implementacja konfiguracji jest w osobnym module, motywacją tego
   jest to że zastanawiałem się nad róznymi sposobami jej implementacji
   – jako pMap albo jako kopie int Array 
*)

open Konfiguracja

(*  
    będziemy używać wyjątków do przerywania przeszukiwania, gdy żądana wartość
    zostaje znaleziona
*)
exception Solution of int

exception Sens of bool


(* 
   funkcja sprawdzająca, czy przy danych pojemnościach szklanek
   możliwa jest w ogóle docelowa konfiguracja:
   sprawdza czy w cel jest 1 szklanka pusta lub 1 szklanka pełna;
   sprawdza, że wszystkie poziomy wody w 'cel' zmieszczą się w szklankach;
   sprawdza, że NWD (cel, pojemnosci) = NWD (pojemnosci)
*)

let ma_sens pojemnosci cel =
    let rec nwd a b = 
        if a < 0 then b else if b <= 0 then a else
        nwd b (a mod b)
    in
    let ok = ref false in
    let nwd_c = ref (-1) in
    let nwd_p = ref (-1) in
    let aux i c =
        let p = (get pojemnosci i) in
        if c > p || c < 0 || p < 0 then raise (Sens false) else
        begin
            nwd_c := nwd (!nwd_c) c;
            nwd_p := nwd (!nwd_p) p;
            if c = 0 || c = p then ok := true
        end
    in
    try begin
        iter aux cel;
        (!ok) && nwd (!nwd_c) (!nwd_p) = !nwd_p
    end with
        Sens(b) -> b

(* poniższe funkcje stanowią pojedyncze modyfikacje konfiguracji *)
let nalej p u s =
    set u s (get p s)

let wylej u s =
    set u s 0

let przelej p u s1 s2 =
    let w1, w2, p1, p2 = get u s1, get u s2, get p s1, get p s2 in
    let wolne_miejsce = p2 - w2 in
    let u = set u s1 (max 0 (w1 - wolne_miejsce)) in
    set u s2 (min p2 (w2 + w1))


(* główna funkcja *)
let przelewanka l =
    let pojemnosci, cel = create2 l in
    
    if l = [||] then 0 else
    
    if not (ma_sens pojemnosci cel) then -1 else
    
    let pocz = map (fun x -> 0) pojemnosci in
    
    let oczekiwana_wlk_tbl l = 100000 in
    (* pomijam zależność od n, nie powinno mieć dużego wpływu na cokolwiek *)
    
    (* w hashtabeli odwiedzone będziemy zbierać informacje o już odwiedzonych
       wierzchołkach grafu *)
    let odwiedzone = ref (Hashtbl.create (oczekiwana_wlk_tbl l)) in
    (* w kolejce q będziemy trzymać jeden poziom grafu *)
    let q = ref (Queue.create ()) in

    (* funkcja znajdująca dzieci wierzchołka u i dodająca je do kolejki nq *)
    let dodaj_nastepne u nq = 
        (* funkcja dodająca, sprawdza czy wierzchołek nie był już dołączony
           do grafu gdzieś wyżej *)
        let sprobuj_dodac nu nq =
            if (not (Hashtbl.mem !odwiedzone nu)) then
            begin
                Queue.add nu !nq;
                Hashtbl.add !odwiedzone nu true
            end
        in

        (* funkcja znajdująca konfiguracje możliwe do uzyskania przy używaniu
           szklanki s *)
        let nast_szkl u nq s w = 
            begin
            sprobuj_dodac (nalej pojemnosci u s) nq;
            (* z pustego i Salomon nie naleje *)
            if w > 0 then begin    
                sprobuj_dodac (wylej u s) nq;
                for i = 0 to (size u - 1) do
                    if i != s then sprobuj_dodac (przelej pojemnosci u s i) nq
                done;
            end;
        end in
        iter (nast_szkl u nq) u
    in
    
    (* główna pętla *)
    let rec aux q acc =
        (* nq będzie zawierać wierzchołki poziom niżej *)
        let nq = ref (Queue.create ()) in
        begin
            while not (Queue.is_empty !q) do
                let el = Queue.pop !q in
                if el = cel then raise (Solution acc) else
                dodaj_nastepne el nq;
            done;
            if Queue.is_empty !nq then -1 else
            aux nq (acc + 1)
        end
    in

    try begin
        Queue.add pocz !q;
        aux q 0
    end with
        | Solution(n) -> n
