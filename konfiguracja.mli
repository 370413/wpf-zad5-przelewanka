type konfiguracja

val get : konfiguracja -> int -> int

val set : konfiguracja -> int -> int -> konfiguracja

val create : int array -> konfiguracja

val create2 : (int * int) array -> (konfiguracja * konfiguracja)

val map : (int -> int) -> konfiguracja -> konfiguracja

val iter : (int -> int -> unit) -> konfiguracja -> unit

val size : konfiguracja -> int
